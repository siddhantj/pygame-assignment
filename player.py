import pygame
import time

# Read global variables from config file
import configparser
config = configparser.ConfigParser()
config.read('config.ini')

rows = int(config['DEFAULT']['rows'])
game_width = int(config['DEFAULT']['game_width'])

p_height = int(config['DEFAULT']['p_height'])
p_width = int(config['DEFAULT']['p_width'])

stat_obs_height = int(config['DEFAULT']['stat_obs_height'])
stat_obs_width = stat_obs_height

mov_obs_width = int(config['DEFAULT']['mov_obs_width'])
mov_obs_height = 2*stat_obs_height


game_height = 2*p_height + rows*(mov_obs_height+stat_obs_height)
# All config variables are read


class Player(object):
    # class Player contains all the data about each player
    def __init__(self, score, x, y, going_dir, im_link, win):
        # Constructor
        self.score = score
        self.x = x
        self.y = y
        self.going_dir = going_dir
        self.win = win
        self.height = p_height
        self.width = p_width
        self.color = (0, 0, 0)
        self.img = im_link
        self.image = pygame.image.load(self.img)
        self.image = pygame.transform.scale(self.image, (p_width, p_height))

    def draw(self, dirn):
        # Draw player after updating location
        if dirn == 'up':
            if self.going_dir == 1:
                self.score += 5
            else:
                self.score -= 5
            self.y -= self.height
        elif dirn == 'down':
            if self.going_dir == -1:
                self.score += 5
            else:
                self.score -= 5
            self.y += self.height
        elif dirn == 'left':
            self.x -= self.width
        elif dirn == 'right':
            self.x += self.width

        if self.x < 0:
            self.x = 0
        if self.x+self.width > game_width:
            self.x = game_width - self.width
        if self.y < 0:
            self.y = 0
        if self.y + self.height > game_height:
            self.y = game_height - self.height

        self.win.blit(self.image, (self.x, self.y))
        pygame.display.update()

    def new_draw(self):
        # draw function to be used if the player wins or crashes
        self.image = pygame.image.load(self.img)
        self.image = pygame.transform.scale(self.image, (p_width, p_height))
        self.win.blit(self.image, (self.x, self.y))
        pygame.display.update()
        time.sleep(1)

        # time.sleep(0.1)
        # self.win.blit(self.image, (self.x, self.y))
    def rect(self):
        # return rect to check for collision
        return pygame.Rect(self.x, self.y, self.width, self.height)

    def get_y(self):
        return self.y

    def get_score(self):
        # methood to return score of player
        return self.score

    def upd_score(self, delta):
        self.score += delta
