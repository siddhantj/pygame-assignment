import configparser
import pygame
from random import seed
from random import randint
from player import Player
from obstacle import Obstacle
from datetime import datetime
import time
seed(datetime.now())


def hexToColour(hash_colour):
    red = int(hash_colour[1:3], 16)
    green = int(hash_colour[3:5], 16)
    blue = int(hash_colour[5:7], 16)
    return (red, green, blue)


# Read global variables from config file
config = configparser.ConfigParser()
config.read('config.ini')

rows = int(config['DEFAULT']['rows'])
game_width = int(config['DEFAULT']['game_width'])
footer = int(config['DEFAULT']['footer'])
p_height = int(config['DEFAULT']['p_height'])
p_width = int(config['DEFAULT']['p_width'])

stat_obs_height = int(config['DEFAULT']['stat_obs_height'])
stat_obs_width = stat_obs_height

mov_obs_width = int(config['DEFAULT']['mov_obs_width'])
mov_obs_height = 2*stat_obs_height

game_height = 2*p_height + rows*(mov_obs_height+stat_obs_height)

text_font = config['TEXT']['font']
# All config variables are read


class Level():
    # Class Level is responsible for randomly creating each level
    # difficulty of each level is determined by level number
    def __init__(self, lev_num, win, p1_init_score, p2_init_score):
        # Constructor
        self.lev_num = lev_num
        self.win = win
        self.curr_mov = 1
        self.create_level_attributes(p1_init_score, p2_init_score)
        self.game_stat = 0

    def create_level_attributes(self, p1_init_score, p2_init_score):
        # Create obstacles, the players for each level and give them
        # the required attributes
        self.stat_obs = []
        self.mov_obs = []
        self.p1 = Player(p1_init_score, randint(
            0, game_width-p_width), game_height-p_height,
            1, "img/p1.png", self.win)
        self.p2 = Player(p2_init_score, randint(
            0, game_width-p_width), 0, -1, "img/p2.png", self.win)

        num_stat = max(int(2*self.lev_num), 5)
        num_mov = max(self.lev_num*2+1, 4)

        for _ in range(0, num_stat):
            # Create stationary obstacles
            row_num = randint(0, rows-1)
            pos_y = p_height + (mov_obs_height+stat_obs_height) * \
                row_num + mov_obs_height
            r = randint(
                0, game_width-stat_obs_width)
            self.stat_obs.append(Obstacle(
                stat_obs_height, stat_obs_width, r, pos_y, 0, 0, self.win))

        for i in range(0, num_mov):
            # Create moving obstacles
            row_num = randint(0, rows-1)
            cnt = 0
            for obs in self.mov_obs:
                if obs.row_num == row_num:
                    cnt += 1
            if cnt > 3:
                i -= 1
                continue
            pos_y = p_height + (mov_obs_height+stat_obs_height) * \
                row_num + mov_obs_height/4
            vel = int(self.lev_num)+3
            dirn = randint(0, 1)
            if dirn == 0:
                vel *= -1
            r = randint(
                0, game_width-mov_obs_width)
            obstakle = Obstacle(mov_obs_height/2, mov_obs_width,
                                r, pos_y, vel, row_num, self.win)
            self.mov_obs.append(obstakle)

        self.p1.start_time = time.time()

    def next_frame(self, key_inp, who_moves):
        # Create the next frame after updating the player position
        self.win.fill((randint(0, 10), randint(0, 10), randint(0, 10)))

        pygame.draw.rect(self.win, hexToColour(config['COLOR']['st_end']),
                         (0, 0, game_width, p_height))
        pygame.draw.rect(self.win, (255, 255, 255),
                         (0, game_height, game_width, footer))
        pygame.draw.rect(self.win, hexToColour(config['COLOR']['st_end']),
                         (0, game_height-p_height, game_width, p_height))

        font = pygame.font.Font(text_font, 32)
        lvl_text = "Level: " + str(self.lev_num - 3)
        text = font.render(lvl_text, True, (0, 0, 0))
        self.win.blit(text, (5, game_height+5))

        left_space_for_text = 200

        p1_sc_text = "Player 1 score: " + str(self.p1.get_score())
        text = font.render(p1_sc_text, True, (0, 0, 0))
        self.win.blit(text, (left_space_for_text, game_height+5))

        p2_sc_text = "Player 2 score: " + str(self.p2.get_score())
        text = font.render(p2_sc_text, True, (0, 0, 0))
        self.win.blit(text, (left_space_for_text, game_height+50))

        for i in range(0, rows):
            # Render rows
            pos_y = p_height + i*(stat_obs_height+mov_obs_height)
            pygame.draw.rect(self.win, hexToColour(config['COLOR']['river']),
                             (0, pos_y, game_width, mov_obs_height))
            pos_y += mov_obs_height
            pygame.draw.rect(self.win, hexToColour(config['COLOR']['safe_space']),
                             (0, pos_y, game_width, stat_obs_height))

        # render obstacles
        for obs in self.stat_obs:
            obs.draw()

        for obs in self.mov_obs:
            obs.draw()

        if(self.curr_mov == 1):

            # move for player one
            if who_moves == 1:
                self.p1.draw(key_inp)
            else:
                self.p1.draw('na')
            temp = time.time()
            if temp - self.p1.start_time > 0.7:
                self.p1.upd_score(-1)
                self.p1.start_time = temp
            hit = False
            for obs in self.stat_obs:
                if(self.p1.rect().colliderect(obs.rect())):
                    hit = True
                    # print("asda")
                    break

            for obs in self.mov_obs:
                if(self.p1.rect().colliderect(obs.rect())):
                    hit = True
                    # print("asda")
                    break

            if hit:
                self.p1.upd_score(-20)
                self.p1.end_time = time.time()
                self.curr_mov = 2
                self.p1.img = "img/loss.png"
                self.p1.new_draw()

                self.p2.start_time = time.time()

            if self.p1.get_y() < 1:
                self.p1.upd_score(10)
                self.curr_mov = 2
                self.p1.img = "img/win.png"
                self.p1.new_draw()

                self.p2.start_time = time.time()
        else:
            # move for player 2
            if who_moves == 2:
                self.p2.draw(key_inp)
            else:
                self.p2.draw('na')
            temp = time.time()
            if temp - self.p2.start_time > 0.7:
                self.p2.upd_score(-1)
                self.p2.start_time = temp
            hit = False
            for obs in self.stat_obs:
                if(self.p2.rect().colliderect(obs.rect())):
                    hit = True
                    # print("asda")
                    break

            for obs in self.mov_obs:
                if(self.p2.rect().colliderect(obs.rect())):
                    hit = True
                    # print("asda")
                    break

            if hit:
                self.p2.upd_score(-20)
                self.game_stat = 1

                self.p2.img = "img/loss.png"
                self.p2.new_draw()

            if self.p2.get_y() > game_height-p_height-1:
                self.p2.upd_score(10)
                self.game_stat = 1
                self.p2.img = "img/win.png"
                self.p2.new_draw()

    def get_status(self):
        return self.game_stat
