import pygame
import math
# Read global variables from config file
import configparser
config = configparser.ConfigParser()
config.read('config.ini')

rows = int(config['DEFAULT']['rows'])
game_width = int(config['DEFAULT']['game_width'])

p_height = int(config['DEFAULT']['p_height'])
p_width = int(config['DEFAULT']['p_width'])

stat_obs_height = int(config['DEFAULT']['stat_obs_height'])
stat_obs_width = stat_obs_height

mov_obs_width = int(config['DEFAULT']['mov_obs_width'])
mov_obs_height = 2*stat_obs_height


game_height = 2*p_height + rows*(mov_obs_height+stat_obs_height)
# All config variables are read


class Obstacle():
    # Class obstacles contains all the information about each obstacle
    # botch moving and fixed obstacles are taken care of with one class
    def __init__(self, height, width, x, y, vel, row_num, win):
        # constructor
        self.height = height
        self.width = width
        self.x = x
        self.y = y
        self.x_for_sin = x*100
        self.v_for_sin = 0.1
        self.vel = vel
        self.win = win
        self.row_num = row_num
        self.img = "img/crate.jpeg"
        self.image = pygame.image.load(self.img)
        if vel:
            self.image = pygame.transform.scale(
                self.image, (mov_obs_width, mov_obs_height//2))
        else:
            self.image = pygame.transform.scale(
                self.image, (stat_obs_width, stat_obs_height))

    def draw(self):
        # Draw the obstacle to display and change x axis as per velocity
        if self.vel > 0:
            if self.x + self.width + self.vel > game_width:
                self.vel *= -1
        elif self.vel < 0:
            if self.x + self.vel < 0:
                self.vel *= -1

        self.x_for_sin += self.v_for_sin
        self.x += self.vel
        if self.vel:
            self.y += 1.1*math.sin(self.x_for_sin)
        self.win.blit(self.image, (self.x, self.y))

    def rect(self):
        # return bounding box to check for collisions
        return pygame.Rect(self.x, self.y, self.width, self.height)
