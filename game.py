import pygame
import time
from level import Level
import configparser


def hexToColour(hash_colour):
    # Convert a HTML-hexadecimal colour string to an RGB triple-tuple
    red = int(hash_colour[1:3], 16)
    green = int(hash_colour[3:5], 16)
    blue = int(hash_colour[5:7], 16)
    return (red, green, blue)


# Read global variables from config file
config = configparser.ConfigParser()
config.read('config.ini')

rows = int(config['DEFAULT']['rows'])
game_width = int(config['DEFAULT']['game_width'])
footer = int(config['DEFAULT']['footer'])

p_height = int(config['DEFAULT']['p_height'])
p_width = int(config['DEFAULT']['p_width'])

stat_obs_height = int(config['DEFAULT']['stat_obs_height'])
stat_obs_width = stat_obs_height

mov_obs_width = int(config['DEFAULT']['mov_obs_width'])
mov_obs_height = 2*stat_obs_height

game_height = 2*p_height + rows*(mov_obs_height+stat_obs_height)

text_font = config['TEXT']['font']
# All config variables are read


pygame.init()
# win is the game window
win = pygame.display.set_mode((game_width, game_height+footer))


curr_level = 4
lev = Level(curr_level, win, 0, 0)
paused = False

# MAIN LOOP
run = True
while run:
    pygame.time.delay(10)
    if curr_level > 6:
        # Game end conditions
        win.fill(hexToColour(config['COLOR']['end_fill']))
        winner_output = "Winner: "
        if lev.p1.score > lev.p2.score:
            winner_output = config['TEXT']['p1']
        if lev.p2.score > lev.p1.score:
            winner_output = config['TEXT']['p2']
        if lev.p2.score == lev.p1.score:
            winner_output = config['TEXT']['drew']

        font = pygame.font.Font(text_font, 50)
        text = font.render(
            winner_output, True, hexToColour(config['COLOR']['win_end']))
        win.blit(text, (game_width//2 - 150, game_height//2))
        pygame.display.update()
        time.sleep(5)
        break

    for event in pygame.event.get():
        # Check for key presses
        if event.type == pygame.QUIT:
            run = False
        elif event.type == pygame.KEYDOWN:
            if not paused:
                # Keys for player 1
                if event.key == pygame.K_UP:
                    lev.next_frame("up", 1)
                elif event.key == pygame.K_LEFT:
                    lev.next_frame("left", 1)
                elif event.key == pygame.K_RIGHT:
                    lev.next_frame("right", 1)
                elif event.key == pygame.K_DOWN:
                    lev.next_frame("down", 1)

                # Keys for player 2
                elif event.key == pygame.K_w:
                    lev.next_frame("up", 2)
                elif event.key == pygame.K_a:
                    lev.next_frame("left", 2)
                elif event.key == pygame.K_d:
                    lev.next_frame("right", 2)
                elif event.key == pygame.K_s:
                    lev.next_frame("down", 2)
            if event.key == pygame.K_ESCAPE:
                # toggle pause game
                paused = not paused
    else:
        if not paused:
            lev.next_frame("na", 3)

    if lev.get_status():
        # Level end condition
        curr_level += 1
        lev = Level(curr_level, win, lev.p1.get_score(), lev.p2.get_score())
    pygame.display.update()
