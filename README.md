## Instructions to run
	
run the following command from the root game directory
	

    python3 game.py

 
	

## Controls:
for player one:
	Arrow keys

for player two:
	WASD
	
and esc key to pause

## Design Choices
	

 1. The moving obstacles appear as if floating in water.
 2. All the levels are randomly generated with increasing level of difficulty.
 3. Everything is object oriented.
 4. Score is incremented for crossing each obstacle and deducted for crashing into an obstacle.
 5. The score also reduces with more time taken
 6. Different controls for both the player 
 7. Pause functionality 
 8. Fonts and messages are customisable from the config file.